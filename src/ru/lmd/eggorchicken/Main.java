package ru.lmd.eggorchicken;

public class Main {

    public static void main(String[] args) {

        EggOrChicken egg = new EggOrChicken("Яйцо");
        EggOrChicken chicken = new EggOrChicken("Курица");

        try {
            egg.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (chicken.thread.isAlive()){
            System.out.println("Курица появилась первой!");
        }else{
            System.out.println("Яйцо появилось первым!");
        }
    }

}