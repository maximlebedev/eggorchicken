package ru.lmd.eggorchicken;

public class EggOrChicken implements Runnable {

    Thread thread;

    public EggOrChicken(String name) {
        thread = new Thread(this, name);
        thread.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            try {
                thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(thread.getName());
        }
    }
}
